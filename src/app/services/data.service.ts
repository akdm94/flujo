import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private address = new BehaviorSubject('default message');
  currentAddress = this.address.asObservable();

  constructor() { }

  sendAddress(addressParam) {
    this.address.next(addressParam);
  }
}
