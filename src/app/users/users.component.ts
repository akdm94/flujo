import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UsersService } from '../services/users.service';
import { Observable } from 'rxjs';
import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
import { ModalDetailComponent } from '../modal-detail/modal-detail.component';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  displayedColumns: string[] = ['userId', 'username', 'email', 'details'];
  constructor(private userService: UsersService, private modalService: NgbModal, private data: DataService) {
   }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.userService.getUsers().subscribe(data => {
      this.users = data;
      // console.log(this.users);
    });
  }

  open(addresses: any) {
    const modalRef = this.modalService.open(ModalDetailComponent);
    modalRef.componentInstance.title = 'About';
    // console.log(user, addresses);
    this.data.sendAddress(addresses);
  }
}
