export class Address {
    line1: string;
    locality: string;
    state: string;
    pin: number;
}
