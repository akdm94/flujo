import { Address } from './address.model';

export class User {
  userId: string;
  username: string;
  email: string;
  addresses: Address[];
}
