import { Component, OnInit } from '@angular/core';
import { Address } from '../models/address.model';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-modal-detail',
  templateUrl: './modal-detail.component.html',
  styleUrls: ['./modal-detail.component.css']
})
export class ModalDetailComponent implements OnInit {

  addresses: any;
  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.currentAddress.subscribe(data => this.addresses = data);
    console.log(this.addresses);
  }

}
