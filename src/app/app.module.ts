import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent, FooterComponent } from './shared';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './services/users.service';
import { DataService } from './services/data.service';

import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import { AvatarModule, AvatarConfig } from 'ngx-avatar';
import { ModalComponent } from './modal/modal.component';
import { ModalDetailComponent } from './modal-detail/modal-detail.component';

const avatarColors = [ '#f39c12', '#1abc9c'];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    UsersComponent,
    ModalComponent,
    ModalDetailComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatSelectModule,
    HttpClientModule,
    AppRoutingModule,
    AvatarModule.forRoot({
      colors: avatarColors
    })
  ],
  providers: [UsersService, NgbActiveModal, DataService],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalDetailComponent
  ]
})
export class AppModule { }
